package org.fahai.app;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Cleaner {

	public static String cleanLine(String original) {
		original = original.replaceAll("^\\/\\* +\\d+:.*\\*\\/ ", "");
		original = original
				.replaceAll(
						"/\\*\\s*Location:\\s*\\S*\\s*\\*\\sQualified\\sName:\\s*\\S*\\s*\\*\\sJD-Core\\sVersion:\\s*\\S*\\s*\\*/",
						"");
		return original;
	}

	public boolean cleanDirectory(File inputDir) {
		if (!inputDir.isDirectory()) {
			System.out.println("inputDir is not a directory: " + inputDir.getAbsolutePath());
			return false;
		}
		boolean correct = true;
		String[] fileNames = inputDir.list();
		for (String fileName : fileNames) {
			if ((!".".equals(fileName)) && (!"..".equals(fileName))) {
				File inputFile = new File(inputDir.getAbsolutePath()
						+ File.separator + fileName);
				if (inputFile.isDirectory()) {
					cleanDirectory(new File(inputDir.getAbsolutePath()
							+ File.separator + fileName));
				} else {
					cleanSingleFile(inputFile);
					System.out.println("done: " + inputFile.getAbsolutePath());
				}
			}
		}
		return correct;
	}

	/**
	 * 返回已处理的字符列表
	 * @param file
	 * @return
	 */
	public int cleanSingleFile(File file) {
		List<String> srcLines = new ArrayList<String>();
		List<String> targetLines = new ArrayList<String>();
		srcLines = FileUtil.Lines(file);
		for(String line:srcLines){
			targetLines.add(cleanLine(line));
		}
		return FileUtil.overWrite(file, targetLines);
	}

}