package org.fahai.app;

import java.io.File;

/**
 * 为JD-GUI程序清除行前多余的注释
 * JD-GUI网址：http://jd.benow.ca/
 * 例如：/*  1:   *\/ package org.apache.solr.handler.dataimport;
 * @author fahai
 *
 */
public class Main {

	public static void main(String[] args) {
		Cleaner cleaner = new Cleaner();
		File targetDir = new File(System.getProperty("target.dir"));
		cleaner.cleanDirectory(targetDir);
	}
}
