package org.fahai.app;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 封装了些文件相关的操作
 */
public class FileUtil {
	private static Integer BUFFER_SIZE = 1024 * 1024 * 10;

	/**
	 * 获取文件的行数
	 */
	public static int countLines(File file) throws IOException {
		InputStream is = null;
		int count = 0;
		try {
			is = new BufferedInputStream(new FileInputStream(file));
			byte[] c = new byte[BUFFER_SIZE];
			int readChars = 0;
			while ((readChars = is.read(c)) != -1) {
				for (int i = 0; i < readChars; ++i) {
					if (c[i] == '\n')
						++count;
				}
			}
		} finally {
			is.close();
		}
		return count;
	}

	/**
	 * 以列表的方式获取文件的所有行
	 */
	public static List<String> Lines(File file) {
		BufferedReader reader = null;
		List<String> list = new ArrayList<String>();
		try {
			reader = new BufferedReader(new FileReader(file));
			String line = null;
			while ((line = reader.readLine()) != null) {
				list.add(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * 以列表的方式获取文件的指定的行数数据
	 */
	public static List<String> Lines(File file, int lines) {
		BufferedReader reader = null;
		List<String> list = new ArrayList<String>();
		try {
			reader = new BufferedReader(new FileReader(file));
			String line = null;
			while ((line = reader.readLine()) != null) {
				list.add(line);
				if (list.size() == lines) {
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return list;
	}

	/**
	 * 快速清空一个超大的文件
	 */
	public static boolean cleanFile(File file) {
		FileWriter fw = null;
		try {
			fw = new FileWriter(file);
			fw.write("");
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fw.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	/**
	 * 用给定的字符列表覆盖指定文件
	 */
	public static int overWrite(File file, List<String> lines) {
		if (cleanFile(file)) {
			BufferedWriter writer = null;
			try {
				writer = new BufferedWriter(new FileWriter(file, true));
				for (String singleLine : lines) {
					writer.write(singleLine);
					writer.write(System.getProperty("line.separator"));
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return lines.size();
		} else {
			return 0;
		}
	}

}
